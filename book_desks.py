from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import base64
import datetime
import time
import sys

# ----- CONSTANTS -----
DEBUG = False
DESK = sys.argv[1]
EMAIL = sys.argv[2]
FLOOR_PLAN = DESK.split('.')[0]
FLOOR = int(FLOOR_PLAN[0:2])
HEADLESS = True
PASSWORD = base64.b64decode(sys.argv[3])

# ----- VARIABLES -----
date_format = "%Y-%m-%d"
full_date = (datetime.datetime.today() + datetime.timedelta(days=8)).strftime(date_format)

print 'Full date:', full_date

chrome_options = webdriver.ChromeOptions()

if HEADLESS:
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')

driver = webdriver.Chrome(chrome_options=chrome_options)
print 'Going to desk booking website'
driver.get('https://ncr.condecosoftware.com/Master.aspx')

if len(driver.find_elements_by_xpath("//input[@name='btnRedirectID']")) > 0:
    print 'Found redirect button'
    driver.find_element_by_xpath("//input[@name='btnRedirectID']").click()

time.sleep(3)

print 'Inputting username'
driver.find_element_by_xpath("//input[@type='email']").send_keys(EMAIL)
driver.find_element_by_xpath("//input[@type='submit']").click()

time.sleep(1)

print 'Inputting password'
driver.find_element_by_xpath("//input[@type='password']").send_keys(PASSWORD)
driver.find_element_by_xpath("//input[@type='submit']").click()

time.sleep(7)

print 'Clicking go to app'
driver.find_element_by_xpath("//button[text()='Go to app']").click()

time.sleep(3)

print 'Switching to left navigation frame'
driver.switch_to.frame(driver.find_element_by_xpath("//iframe[@id='leftNavigation']"))
print 'Clicking Desk Booking'
driver.find_element_by_xpath("//div[@id='DeskBookingHeader']").click()

time.sleep(3)

print 'Switching to default content'
driver.switch_to.default_content()

print 'Switching to main display frame'
driver.switch_to.frame(driver.find_element_by_xpath("//iframe[@id='mainDisplayFrame']"))

if DEBUG:
    print 'Setting floor to 7'
    driver.find_element_by_xpath("//select[@id='floorNum']/option[@value='7']").click()

print 'Setting date'
driver.find_element_by_xpath("//select[@id='startDate']/option[contains(@value, '{full_date}')]".format(full_date=full_date)).click()

time.sleep(3)

print 'Clicking floor plan'
driver.find_element_by_xpath("//input[@id='FloorPlanButton']").click()

time.sleep(5)

print 'Switching to floor plan frame'
driver.switch_to.frame(driver.find_element_by_xpath("//iframe[@id='iframeFloorPlan']"))

if not DEBUG:
    print 'Clicking', FLOOR_PLAN
    driver.find_element_by_xpath("//a[text()='GHQ - N {floor_plan}']".format(floor_plan=FLOOR_PLAN)).click()
else:
    print 'Clicking 07D'
    driver.find_element_by_xpath("//a[text()='GHQ - N 07D']").click()

time.sleep(2)

if not DEBUG:
    print 'Clicking', DESK
    driver.find_element_by_xpath("//div[contains(@onclick, '{desk}')]".format(desk=DESK)).click()
else:
    print 'Clicking 07D.30'
    driver.find_element_by_xpath("//div[contains(@onclick, '07D.30')]").click()

time.sleep(2)

print 'Switching windows'
new_window = driver.window_handles[1]
driver.switch_to_window(new_window)

print 'Gathering all dates'
elems = driver.find_elements_by_xpath("//tr[@class='clsWeekGroup_2']//td[@align='center']/input")

for elem in elems:
    if not elem.get_property('checked'):
        elem.click()
    else:
        print 'This checkbox is already checked'

print 'Clicking Submit'
driver.find_element_by_xpath("//input[@id='submitButton']").click()
driver.switch_to.alert.accept()
