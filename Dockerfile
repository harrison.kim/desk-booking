FROM joyzoursky/python-chromedriver:2.7-selenium

WORKDIR /tmp

COPY book_desks.py book_desks.py

CMD ["python", "book_desks.py"]
